<div align="center">

  <h1><code>wasm-pack-template</code></h1>

  <strong>A template for kick starting a Rust and WebAssembly project using <a href="https://github.com/rustwasm/wasm-pack">wasm-pack</a>.</strong>

  <p>
    <a href="https://travis-ci.org/rustwasm/wasm-pack-template"><img src="https://img.shields.io/travis/rustwasm/wasm-pack-template.svg?style=flat-square" alt="Build Status" /></a>
  </p>

  <h3>
    <a href="https://rustwasm.github.io/docs/wasm-pack/tutorials/npm-browser-packages/index.html">Tutorial</a>
    <span> | </span>
    <a href="https://discordapp.com/channels/442252698964721669/443151097398296587">Chat</a>
  </h3>

  <sub>Built with 🦀🕸 by <a href="https://rustwasm.github.io/">The Rust and WebAssembly Working Group</a></sub>
</div>

## About

[**📚 Read this template tutorial! 📚**][template-docs]

This template is designed for compiling Rust libraries into WebAssembly and
publishing the resulting package to NPM.

Be sure to check out [other `wasm-pack` tutorials online][tutorials] for other
templates and usages of `wasm-pack`.

[tutorials]: https://rustwasm.github.io/docs/wasm-pack/tutorials/index.html
[template-docs]: https://rustwasm.github.io/docs/wasm-pack/tutorials/npm-browser-packages/index.html

## 🚴 Usage

### 🐑 Use `cargo generate` to Clone this Template

[Learn more about `cargo generate` here.](https://github.com/ashleygwilliams/cargo-generate)

```
cargo generate --git https://gitee.com/guoyucode/excel_read.git --name my-project
cd my-project
```

### 🛠️ Build with `build`

```
wasm-pack build --target web
```

### 🔬 Test in Browsers with `Use js`

```
1: copy pgk dir files to html dir
2: <input type="file" id="file" multiple="multiple" onchange="read()">

3:
<script>
    function read() {
        var file = document.getElementById('file').files[0]//获取文件流

        /// file: 前端File对象
        /// title_row: 标题在第几行 组合标题: [2,3]; 单行标题: [1]
        /// rows_excluded: 排除多少行数据, 一行, 二行, 三行: [1,2,3];
        /// excluded_keyword: 关键字排除: 在单元格中检测到该关键字读取终止
        read_excel_file(file, [1], [], "合计").then(res => {
            console.log("excel_json:", res);
        }).catch(e => {
            alert(e);
        });
    }
</script>

4:
<script type="module">
    // 初始化wasm里的方法
    import {default as wasm, greet, read_excel_file} from "./excel_read.js";
    wasm().then((module) => {
        window.read_excel_file = read_excel_file;
    });
</script>
```

### 🛠️ run html `run`

```
cd pkg
cargo install miniserve
miniserve --index index.html
```

## 🔋 Batteries Included

* [`wasm-bindgen`](https://github.com/rustwasm/wasm-bindgen) for communicating
  between WebAssembly and JavaScript.
* [`console_error_panic_hook`](https://github.com/rustwasm/console_error_panic_hook)
  for logging panic messages to the developer console.
* [`wee_alloc`](https://github.com/rustwasm/wee_alloc), an allocator optimized
  for small code size.
